#include <stdio.h>
#include <iostream>
#include <mysql.h>
#include <string.h>
#include <memory>
#include <cstdint>
#include <sys/mman.h>
#include <time.h>
#include "app_settings.h"
#include "errorlog.h"

#ifndef DATABASE_MANAGER_H
#define DATABASE_MANAGER_H

class DatabaseManager
{
    public:

    DatabaseManager(MYSQL*);

    MYSQL *connection;
    void connectToDatabase(AppSettings*);
    void closeDatabaseConnection();
    //void terminateDatabaseConnectionWithError();

};

#endif
