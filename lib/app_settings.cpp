#include "app_settings.h"

using namespace std;

void AppSettings::load()
{
    //open and read app_settings.json file
    ifstream settings_file ("./app_settings.json", ios::binary);

    Json::Value root;
    Json::Reader reader; 

    if (settings_file.is_open())
    {
        reader.parse(settings_file,root);
        

        //load modbus settings into modbus map object
        Json::Value modbus;
        modbus = root["modbus"];

        for(Json::Value::iterator it = modbus.begin(); it != modbus.end();it++)
        {
            Json::Value key = it.key();
            Json::Value value = (*it);

            this->modbus_settings.insert ({key.asString(),value.asString()});   
        }



        //load mqtt settings into mqtt map object
        Json::Value mqtt_local;
        mqtt_local = root["messaging_server"];

        for(Json::Value::iterator itm = mqtt_local.begin(); itm != mqtt_local.end(); itm++)
        {
            Json::Value key_m = itm.key();
            Json::Value value_m = (*itm);

            this->mqtt.insert ({key_m.asString(),value_m.asString()});
        }

        //load data logging settings into data_logging map object
        Json::Value data_logging_params;
        data_logging_params = root["log_settings"];

        for(Json::Value::iterator itd = data_logging_params.begin(); itd != data_logging_params.end(); itd++)
        {
            Json::Value key_d = itd.key();
            Json::Value value_d = (*itd);

            this->data_logging.insert ({key_d.asString(),value_d.asString()});
        }

        settings_file.close();

    }
    else
        cout<< "Unable to open Settings Configuration File \n";

}