#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <cstdint>
#include <jsoncpp/json/json.h>
#include <bits/stdc++.h>
#include <map>

#ifndef APP_SETTINGS_H
#define APP_SETTINGS_H

class AppSettings
{
    public:

    std::map <std::string, std::string> modbus_settings;
    std::map <std::string, std::string> mqtt;
    std::map <std::string, std::string> data_logging;

    void load();
};

#endif