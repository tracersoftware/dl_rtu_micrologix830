#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <map>
#include <jsoncpp/json/json.h>

#ifndef TAG_H
#define TAG_H

class Tag
{
    public:
    
    std::string tag_name;
    std::string description;
    std::string io_type; //COIL_OUTPUT, DISCRETE_INPUT, ANALOG_INPUT, HOLDING_REGISTER
    std::string data_type; //REAL, UINT, BOOLEAN
    int modbus_address;
    int register_position_index; //position in modbus data register (see IO_Buffer) 
    bool alarm_event_enable = false;
    float alarm_hihi = 0;
    float alarm_hi = 0;
    float alarm_lo = 0;
    float alarm_lolo = 0;
    std::string event_msg_true = "N/A";
    std::string event_msg_false = "N/A";
    int alarm_priority = 0;
    bool log_enable = false;


    

};

#endif

