#include "data_logger.h"

using namespace std;


bool DataLogger::saveData(DatabaseManager * database_manager,vector<LogRecord> records)
{
    if(!records.empty())
    {
       
        std::string query;
        vector<LogRecord>::iterator itr;

        for (itr = records.begin(); itr < records.end(); itr++)
        {
            query = query + "('"+itr->tag_name+"','"+itr->tag_value+"','"+itr->timestamp+"'),";
        }

        query.pop_back(); //remove last comma from query string
       
        query = "INSERT INTO data_logger_tbl (tag_name,tag_value,data_timestamp) VALUES " + query;

        if(mysql_query(database_manager->connection, query.c_str()) != 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }

}

bool DataLogger::performCleanUp(DatabaseManager * database_manager,int data_age)
{
    std::string query = "DELETE FROM data_logger_tbl WHERE data_timestamp < now() - interval "+std::to_string(data_age)+" DAY";

    if(mysql_query(database_manager->connection, query.c_str()) != 0) 
    {
        ErrorLog::writeToLog("Unable to perform IO Data Log Cleanup of older records");
        return false;
    }
    else
        return true;

}

bool DataLogger::clearAllLogs(DatabaseManager * database_manager)
{
    std::string query = "DELETE FROM data_logger_tbl";

    if(mysql_query(database_manager->connection, query.c_str()) != 0) 
    {
        ErrorLog::writeToLog("Unable to clear IO Data Log Storage");
        return false;
    }
    else
        return true;

}

Json::Value DataLogger::retrieveLogs(DatabaseManager * database_manager,std::string tag_name,std::string start_date,std::string end_date)
{
    Json::Value historic_data;

    std::string query = "SELECT tag_name,tag_value,data_timestamp FROM data_logger_tbl WHERE tag_name = '"+tag_name+"' AND (data_timestamp BETWEEN '"+start_date+"' AND '"+end_date+"') ORDER BY data_timestamp DESC";

    if(mysql_query(database_manager->connection, query.c_str())) 
    {
        cout<<"Retrieve Logs Query Failed"<<endl;
        ErrorLog::writeToLog("Retrieve Logs Query Failed");
        return historic_data;
    }
    else
    {
        MYSQL_RES *result = mysql_store_result(database_manager->connection);//get result set
        int num_of_rows = mysql_num_rows(result);//get number of rows in result set

        MYSQL_ROW row;

        while (row = mysql_fetch_row(result))
        {
            //std::string tagname(row[0]);
            //std::string tag_value(row[1]);
            //std::string data_timestamp(row[2]);

            Json::Value record;
            record["tag_name"] = (std::string) row[0];
            record["tag_value"] = (std::string) row[1];
            record["data_timestamp"] = (std::string) row[2];

            historic_data["tag_historic_data"].append(record);
        }

        return historic_data;

    }

}
