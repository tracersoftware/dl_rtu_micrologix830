#include "command_manager.h"

using namespace std;

CommandManager::CommandManager(AppSettings *_app_settings,IO_Buffer *_io_buffer, ModbusDeviceReader *_modbus_device_reader, DataLogger * _data_logger,DatabaseManager * _database_manager)
{
    this->app_settings = _app_settings;
    this->io_buffer    = _io_buffer;
    this->modbus_device_reader = _modbus_device_reader;
    this->data_logger = _data_logger;
    this->database_manager = _database_manager;
    //this->mqtt_client = _mqtt_client;
    
}

bool CommandManager::write_to_tag(Json::Value root)
{
    int address; int tag_value; float tag_value_float;
    
    //get tag name
    std::string tag_name = root["command_data"]["tag_name"].asString();
    
    
    if(this->io_buffer->tag_database[tag_name].io_type == "COIL_OUTPUT")
    {
        address = this->io_buffer->tag_database[tag_name].modbus_address;// get tag Modbus address
        tag_value = stoi(root["command_data"]["value"].asString());//get tag value to write

        if(this->modbus_device_reader->writeDiscreteIO((address - 1),tag_value))
         return true;
        else
         return false;
    }
    else if(this->io_buffer->tag_database[tag_name].io_type == "HOLDING_REGISTER")
    {
        //if data type is UINT
        if(this->io_buffer->tag_database[tag_name].data_type == "UINT")
        {
            address = this->io_buffer->tag_database[tag_name].modbus_address;// get tag Modbus address
            tag_value = stoi(root["command_data"]["value"].asString());//get tag value to write

            if(this->modbus_device_reader->writeAnalogIO((address - 1),tag_value))
             return true;
            else
             return false;
        }
        else if(this->io_buffer->tag_database[tag_name].data_type == "REAL")
        {
            address = this->io_buffer->tag_database[tag_name].modbus_address;// get tag Modbus address
            tag_value_float = stof(root["command_data"]["value"].asString());//get tag value to write

            if(this->modbus_device_reader->writeAnalogIO_Float((address-1),tag_value_float,this->modbus_device_reader->little_endianness_enable))
             return true;
            else
             return false;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void CommandManager::tag_db_management(Json::Value root,MQTT_Client * mqtt_client)
{
    //get command/function
    std::string command_name = root["command_data"]["command"].asString();

    if(command_name == "get_db")
    {
        this->tag_db_data = this->io_buffer->get_tag_db_JSON_Formatted();
        std::string db_data = this->tag_db_data;
        mqtt_client->send_message_to_any_topic(db_data.c_str(),this->app_settings->mqtt["mqtt_pub_channel"].c_str());
    }
    else if(command_name == "update_db")
    {
        //call function to update tag_database.json file
        //return "Tag Database Updated";
    }
    else
    {
        //do nothing for now
        //return "";
    }
}

bool CommandManager::run_system_command(Json::Value root)
{
    //get command/function
    std::string command_name = root["command_data"]["command"].asString();

    if(command_name == "reboot")
    {
        system("reboot");
        return true;
    }
    else if(command_name == "set_rtu_time")
    {
        std::string date_value = root["command_data"]["arg1"].asString();
        std::string set_date_time_command = "date --set=\""+date_value+"\"";
        system(set_date_time_command.c_str());
        return true;
    }
    else
    {
        return false;
    }
}

void CommandManager::program_logs_management(Json::Value root,MQTT_Client * mqtt_client)
{
    //get command/function
    std::string command_name = root["command_data"]["command"].asString();

    if(command_name == "get_firmware_program_logs")
    {
        Json::Value root;

        this->errorlog_data = ErrorLog::readAll();
        
        root["error_logs"] = this->errorlog_data;
        std::string error_data = root.toStyledString();

        mqtt_client->send_message_to_any_topic(error_data.c_str(),this->app_settings->mqtt["mqtt_pub_channel"].c_str());
    }
    else if(command_name == "clear_firmware_program_logs")
    {
        ErrorLog::clearAll();
    }
    else
    {
        //do nothing
    }
}

void CommandManager::historical_io_data_management(Json::Value root,MQTT_Client * mqtt_client)
{
    //get command/function
    std::string command_name = root["command_data"]["command"].asString();

    if(command_name == "get_tag_historical_data")
    {
        std::string tag_name = root["command_data"]["command_args"]["tag_name"].asString();
        std::string start_date = root["command_data"]["command_args"]["start_date"].asString();
        std::string end_date = root["command_data"]["command_args"]["end_date"].asString();

        Json::Value historical_tag_data_json;
        historical_tag_data_json = this->data_logger->retrieveLogs(this->database_manager,tag_name,start_date,end_date);
        this->historical_db_data = historical_tag_data_json.toStyledString();
        std::string db_data = this->historical_db_data;

        mqtt_client->send_message_to_any_topic(db_data.c_str(),this->app_settings->mqtt["mqtt_pub_channel"].c_str());
    }
    else if(command_name == "clear_tag_historical_data")
    {
        this->data_logger->clearAllLogs(this->database_manager);
    }
}

