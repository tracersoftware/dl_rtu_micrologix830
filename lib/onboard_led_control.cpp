#include "onboard_led_control.h"

void OnboardLED::triggerHeartbeatMode(int led_id)
{
    if(led_id == ACT_LED)
    {
        system("echo heartbeat | sudo tee /sys/class/leds/led0/trigger");
    }
    else if(led_id == PWR_LED)
    {
        system("echo heartbeat | sudo tee /sys/class/leds/led1/trigger");
    }
    else
    {
        //do nothing
    }

}

void OnboardLED::triggerDefaultMode(int led_id)
{
    if(led_id == ACT_LED)
    {
        system("echo cpu0 | sudo tee /sys/class/leds/led1/trigger");
    }
    else if(led_id == PWR_LED)
    {
        system("echo input | sudo tee /sys/class/leds/led0/trigger");
    }
}