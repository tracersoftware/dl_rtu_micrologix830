#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <map>
#include <vector>
#include <algorithm>
#include <time.h>
#include <mysql.h>
#include "log_record.h"
#include <jsoncpp/json/json.h>
#include "database_manager.h"

#ifndef DATA_LOGGER_H
#define DATA_LOGGER_H

class DataLogger
{
    public:

    bool saveData(DatabaseManager *, vector<LogRecord>);
    bool performCleanUp(DatabaseManager *,int data_age);
    bool clearAllLogs(DatabaseManager *);
    Json::Value retrieveLogs(DatabaseManager *,std::string tag_name,std::string start_date,std::string end_date);

    
};

#endif