#include "errorlog.h"

using namespace std;

vector<std::string> ErrorLog::logs_buffer = {};
bool ErrorLog::write_to_log_enable = true;


void ErrorLog::writeToLog(std::string log_message)
{
    std::string formatted_log;
    formatted_log = getCurrentTime() + ": " + log_message;

    int num_of_write_attempts = 0;//number of times an attempt will be made to write to the logs while waiting for writing function to be enabled

    while(num_of_write_attempts < 10)
    {
        //if writing functionality is enabled, write to buffer
        if(write_to_log_enable)
        {
            logs_buffer.push_back(formatted_log);
            break;
        }
    }
    
    
}

void ErrorLog::writeToDisk()
{
    int buffer_size = logs_buffer.size();

     if(buffer_size > 0)
     {

            fstream logfile ("./error_log.log", std::fstream::in | std::fstream::out | std::fstream::app);

            if(logfile.is_open())
            {
                
                write_to_log_enable = false;//disable writing to buffer

                for(unsigned i = 0; i < buffer_size; i++)
                {
                    logfile << logs_buffer[i] <<std::endl;
                }

                logfile.close();

                logs_buffer.erase(logs_buffer.begin(),logs_buffer.begin()+buffer_size);
                write_to_log_enable = true;


            }

     }
}

string ErrorLog::readAll()
{
    std::string data;
    ifstream in("./error_log.log");
    getline(in, data, string::traits_type::to_char_type(
                      string::traits_type::eof()));
    return data;

}

void ErrorLog::clearAll()
{
    write_to_log_enable = false;
    std::ofstream ofs ("./error_log.log", std::ios::out | std::ios::trunc); // clear contents
    ofs.close();
    write_to_log_enable = true;
}

std::string ErrorLog::getCurrentTime()
{
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);

    strftime(buf, sizeof(buf),"%Y-%m-%d %X", &tstruct);

    return buf;
}