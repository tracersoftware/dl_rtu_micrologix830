#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <string>
#include <stdlib.h>
#include <errno.h>
#include <modbus/modbus-tcp.h>
#include "app_settings.h"
#include "errorlog.h"

#ifndef MODBUS_DEVICE_READER_H
#define MODBUS_DEVICE_READER_H

class ModbusDeviceReader
{
   public:

   modbus_t* ctx;
   uint32_t old_response_to_sec;
   uint32_t old_response_to_usec;
   AppSettings* _app_settings;
   //IO_Buffer* _io_buffer;

   //registers to hold read modbus values from PLC
   uint8_t io_digital_inputs[100]; //starts from 00000
   uint8_t io_coil_outputs[100];   //starts from 10000
   uint16_t io_analog_inputs[100]; //starts from 30000
   uint16_t io_holding_registers[100]; //starts from 40000

   bool little_endianness_enable;
   

   ModbusDeviceReader(modbus_t*,AppSettings*);
   void createDevice();
   void initDevice();
   bool connect(); 
   void disconnect();
   bool readDiscreteIO(std::string type,int,int,uint8_t*); //C = output coil, I = digital inputs
   bool readAnalogIO(std::string type,int,int,uint16_t*); //H = holding registers, I = input registers
   bool writeDiscreteIO(int,int);
   bool writeAnalogIO(int,int);
   bool writeAnalogIO_Float(int,float,bool);//boolean argument explained:- true = swap words, false = don't swap words

   float get_float(uint16_t,uint16_t,bool); //boolean argument explained:- true = swap words, false = don't swap words

};

#endif
