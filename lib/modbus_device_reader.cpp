#include "modbus_device_reader.h"

ModbusDeviceReader::ModbusDeviceReader(modbus_t* context, AppSettings* app_settings)
{
    ctx = context;
    _app_settings = app_settings;
    _app_settings->load();

    //set little_endian_enable flag
    if(_app_settings->modbus_settings["little_endian_enable"] == "true")
     this->little_endianness_enable = true;
    else
     this->little_endianness_enable = false;

    /* _io_buffer = io_buffer;
    _io_buffer->init_addressing_from_config_file(); */
}

void ModbusDeviceReader::createDevice()
{
   std::string ip_address = (std::string) this->_app_settings->modbus_settings["ip_address_plc"];
   int port = std::stoi(this->_app_settings->modbus_settings["port_number"]);
   //cout<<ip_address<<"----"<<port<<endl; //this->_app_settings->modbus_settings["port_number"];
   ctx = modbus_new_tcp(ip_address.c_str(),port);
   
   if(ctx == NULL)
   {
        char buffer [200];
        sprintf(buffer, "Could not connect to MODBUS Network: %s\n", modbus_strerror(errno));
        throw buffer; 

   }

}

bool ModbusDeviceReader::connect()
{
    int status = -1;

    status = modbus_connect(ctx);//try connecting to Modbus network or bus 
    if(status == 0)
    {
        return true;
    }
    else
    {
        //modbus_free(ctx);
        return false;
    }
}

void ModbusDeviceReader::disconnect()
{
    modbus_close(ctx);
    modbus_free(ctx);
}

void ModbusDeviceReader::initDevice()
{
   modbus_set_debug(ctx, TRUE);
   //modbus_set_slave(ctx,MODBUS_BROADCAST_ADDRESS);
   int device_id = std::stoi(this->_app_settings->modbus_settings["device_id"]);
   modbus_set_slave(ctx,device_id);

   /* Save original timeout */
   modbus_get_response_timeout(ctx, &old_response_to_sec, &old_response_to_usec);

   /* Define a new timeout of 1000ms */
   modbus_set_response_timeout(ctx, 0, 1000000);

   modbus_set_error_recovery(ctx, MODBUS_ERROR_RECOVERY_LINK);
}

bool ModbusDeviceReader::readDiscreteIO(std::string type,int starting_addr,int num_of_reg,uint8_t* data_register)
{
    int read_status;
    if(type == "C")
    {
        read_status = modbus_read_bits(ctx,(starting_addr),num_of_reg,data_register);
        if(read_status == -1)
        {
            cout<<"Modbus Read Command (COILS) failed"<<endl;
            return false;
        }
        else
            return true;
    }
    else//type == I
    {
        read_status = modbus_read_input_bits(ctx,(starting_addr),num_of_reg,data_register);
        if(read_status == -1)
        {
            cout<<"Modbus Read Command (DIGITAL INPUTS) failed"<<endl;
             return false;
        }
        else
             return true;
    }
}

bool ModbusDeviceReader::readAnalogIO(std::string type,int starting_addr,int num_of_reg,uint16_t* data_register)
{
    int read_status;

    if(type == "H")
    {
        read_status = modbus_read_registers(ctx,starting_addr,num_of_reg,data_register); 
        if(read_status == -1)
        {
            cout<<"Modbus Read Command (HOLDING REGISTERS) failed"<<endl;
            return false;
        }
        else
            return true;
    }
    else //type = I (Analog Inputs)
    {
        read_status = modbus_read_input_registers(ctx,starting_addr,num_of_reg,data_register);
        if(read_status == -1)
        {
            cout<<"Modbus Read Command (ANALOG INPUTS) failed"<<endl;
            return false;
        }
        else
            return true;
    }

    
}

bool ModbusDeviceReader::writeDiscreteIO(int addr,int status)
{
    int write_status;

    write_status = modbus_write_bit(ctx,addr,status);
    if(write_status == -1)
    {
        cout<<"Modbus Write Command (COIL) failed"<<endl;
        ErrorLog::writeToLog("Modbus Write Command (COIL) failed");
        return false;
    }
    else
        return true;

}

bool ModbusDeviceReader::writeAnalogIO(int addr,int value)
{
    int write_status;

    write_status = modbus_write_register(ctx,addr,value);
    if(write_status == -1)
    {
        cout<<"Modbus Write Command (Holding Register) failed"<<endl;
        ErrorLog::writeToLog("Modbus Write Command (Holding Register) failed");
        return false;
    }
    else
        return true;

}

bool ModbusDeviceReader::writeAnalogIO_Float(int addr,float value,bool swap_words_flag)
{
    uint16_t modbus_write_real[2]; 
    uint16_t temp;

    if(swap_words_flag)
    {
        modbus_set_float(value,modbus_write_real);
        
        temp = modbus_write_real[0];
        modbus_write_real[0] = modbus_write_real[1];
        modbus_write_real[1] = temp;

        if(modbus_write_registers(ctx,addr,2,modbus_write_real) == -1)
        {
          return false;
        }
        else
          return true; 
    }
    else
    {
        modbus_set_float(value,modbus_write_real);

        if(modbus_write_registers(ctx,addr,2,modbus_write_real) == -1)
        {
            return false;
        }
        else
            return true;
    }
}

float ModbusDeviceReader::get_float(uint16_t reg_index_1,uint16_t reg_index_2,bool swap_words_flag)
{
     uint16_t modbus_return[2];//for performing byte swaps and/or combinations to produce float values from Modbus registers

    if(swap_words_flag)
    {
        modbus_return[0] = reg_index_2;modbus_return[1] = reg_index_1;
        return modbus_get_float(modbus_return);
    }
    else
    {
        modbus_return[0] = reg_index_1;modbus_return[1] = reg_index_2;
        return modbus_get_float(modbus_return);
    }
}



