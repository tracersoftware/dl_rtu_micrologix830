#pragma once

#ifndef MQTT_CLIENT_H
#define MQTT_CLIENT_H
#include <stdio.h>
#include <iostream>
#include <mosquittopp.h>
#include <string.h>
#include <unistd.h>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <jsoncpp/json/json.h>
#include "app_settings.h"
#include "onboard_led_control.h"
#include "errorlog.h"
#include "command_manager.h"


class CommandManager;


#define MAX_PAYLOAD 1000

class MQTT_Client : public mosqpp::mosquittopp
{
   private:

    const char * host;
    const char * id;
    const char * topic;
    int port;
    int keepalive;
    
    void on_connect(int rc);
    void on_disconnect(int rc);
    void on_publish(int mid);
    void on_message(const struct mosquitto_message *message);
    void on_subscribe(int mid, int qos_count, const int *granted_qos);

   public:
    
    bool isClientConnected;
    
    MQTT_Client(const char *_id, const char *_topic, const char *_host,int port,AppSettings *_app_settings,CommandManager *_command_manager);
    ~MQTT_Client();
    bool send_message(const char *_message);
    bool send_message_to_any_topic(const char *_message, const char *_topic);
    //void set_init_params(AppSettings *_app_settings,CommandManager *_command_manager);
   
   AppSettings *app_settings;
   CommandManager *command_manager;
};

#endif
