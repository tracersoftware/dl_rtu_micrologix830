#pragma once 

#ifndef COMMAND_MANAGER_H
#define COMMAND_MANAGER_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <memory>
#include <cstdint>
#include <jsoncpp/json/json.h>
#include <bits/stdc++.h>
#include <map>
#include "io_buffer.h"
#include "modbus_device_reader.h"
#include "tag.h"
#include "app_settings.h"
#include "errorlog.h"
#include "data_logger.h"
#include "mqtt_client.h"


class MQTT_Client;


class CommandManager
{
   private:

   ModbusDeviceReader *modbus_device_reader;
   IO_Buffer *io_buffer;
   AppSettings *app_settings; 
   DataLogger * data_logger;
   DatabaseManager * database_manager;
   //MQTT_Client * mqtt_client;
   

   public:
   
   CommandManager(AppSettings *_app_settings,IO_Buffer *_io_buffer, ModbusDeviceReader *_modbus_device_reader,DataLogger *_data_logger,DatabaseManager *_database_manager);
   bool write_to_tag(Json::Value);
   void tag_db_management(Json::Value,MQTT_Client * mqtt_client);
   bool run_system_command(Json::Value);
   void program_logs_management(Json::Value,MQTT_Client * mqtt_client);
   void historical_io_data_management(Json::Value,MQTT_Client * mqtt_client);

   std::string errorlog_data;
   std::string tag_db_data;
   std::string historical_db_data;

};

#endif