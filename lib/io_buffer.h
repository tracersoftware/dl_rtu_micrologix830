#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <map>
#include <vector>
#include <algorithm>
#include <time.h>
#include <jsoncpp/json/json.h>
#include "tag.h"
#include "modbus_device_reader.h"
#include "log_record.h"
#include "errorlog.h"

#ifndef IO_BUFFER_H
#define IO_BUFFER_H


class IO_Buffer
{
   private:

   /* uint8_t discrete_io[100];
   uint16_t analog_io_uint[100];
   float analog_io[100]; */
   int coils_register_index_pos;
   int discrete_input_register_index_pos;
   int analog_register_index_pos;
   int holding_register_index_pos;

   enum io_type_code {DISCRETE_INPUT,
                      COIL_OUTPUT,
                      ANALOG_INPUT,
                      HOLDING_REGISTER,
                      UNKNOWN_TYPE};

   IO_Buffer::io_type_code hash_io_type_code(std::string);
   std::string getCurrentTime();

   template <typename T>
   Json::Value process_analog_tag_alarm_state(T,Tag*);

   Json::Value process_discrete_tag_event_state(int,Tag*);

   public:

   /* std::map <std::string, int> buffer_discrete_input_addressing;
   std::map <std::string, int> buffer_analog_input_addressing; 
   std::map <std::string, int> discrete_output_addressing;
   std::map <std::string, int> analog_output_addressing;  */
   

   int coil_register_count;
   int discrete_input_count;
   int analog_input_count;
   int holding_register_count;

   std::map <std::string, int> modbus_register_start_addr;
   std::map <std::string, Tag> tag_database;
   vector<Tag> tags_to_read;
   vector<LogRecord> logs_to_save;

   std::string tag_values_json;
   std::string alarm_records_json;
   
   IO_Buffer();
   void init_addressing_from_config_file();
   void read_PLC_Data(ModbusDeviceReader*);
   void process_PLC_Data_for_HMI(ModbusDeviceReader*);
   std::string get_tag_db_JSON_Formatted();
   /* void sync_discrete_io_from_PLC();
   void sync_analog_io_from_PLC(); */

};

#endif