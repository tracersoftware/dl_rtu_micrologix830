#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <map>
#include <vector>
#include <algorithm>
#include <time.h>


#ifndef LOG_RECORD_H
#define LOG_RECORD_H

class LogRecord
{
    public:

    std::string tag_name;
    std::string tag_value;
    std::string timestamp;
};

#endif