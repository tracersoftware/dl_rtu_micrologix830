#include "io_buffer.h"
//#include "modbus_device_reader.cpp"

using namespace std;

IO_Buffer::IO_Buffer()
{
   //reset register index positions
   this->coils_register_index_pos = 0;
   this->discrete_input_register_index_pos = 0;
   this->analog_register_index_pos = 0;
   this->holding_register_index_pos = 0;
}

void IO_Buffer::init_addressing_from_config_file()
{
    //open and read app_settings.json file
    ifstream tag_db_file ("./tag_database.json", ios::binary);

    Json::Value root;
    Json::Reader reader; 

    if (tag_db_file.is_open())
    {
        reader.parse(tag_db_file,root);


        //load tag database and tags_for_reading strutures from tag database json file
        int reg_index_ctr = 0;
        
        //discrete input tags
        Json::Value tag_db_di;
        tag_db_di = root["plc_tag_db_modbus_map"]["discrete_inputs"];

        int discrete_io_ctr = 0;

        for(Json::Value::ArrayIndex i = 0; i != tag_db_di.size(); i++)
        {
            Tag tag;
            tag.tag_name    = tag_db_di[i]["tag_name"].asString();
            tag.description = tag_db_di[i]["description"].asString();
            tag.io_type     = tag_db_di[i]["io_type"].asString();
            tag.data_type   = tag_db_di[i]["data_type"].asString();
            tag.modbus_address = tag_db_di[i]["modbus_address"].asInt();
            tag.alarm_event_enable = tag_db_di[i]["alarm_event_enable"].asBool();
            tag.alarm_hihi = tag_db_di[i]["alarm_hihi"].asFloat();
            tag.alarm_hi = tag_db_di[i]["alarm_hi"].asFloat();
            tag.alarm_lo = tag_db_di[i]["alarm_lo"].asFloat();
            tag.alarm_lolo = tag_db_di[i]["alarm_lolo"].asFloat();
            tag.event_msg_true = tag_db_di[i]["event_msg_true"].asString();
            tag.event_msg_false = tag_db_di[i]["event_msg_false"].asString();
            tag.alarm_priority = tag_db_di[i]["alarm_priority"].asInt();
            tag.log_enable = tag_db_di[i]["log_enable"].asBool();

            tag.register_position_index = reg_index_ctr;

            this->tag_database.insert({tag_db_di[i]["tag_name"].asString(),tag}); //add tag to tag_database map structure
            this->tags_to_read.push_back(tag);

            discrete_io_ctr++;

            reg_index_ctr++;

        }
        
        //set number of tags count in register
        this->discrete_input_count = discrete_io_ctr;
        discrete_io_ctr = 0;//reset counter
    
        //output coil tags
        Json::Value tag_db_oc;
        tag_db_oc = root["plc_tag_db_modbus_map"]["coil_outputs"];

        int coil_io_ctr = 0;

        for(Json::Value::ArrayIndex j = 0; j != tag_db_oc.size(); j++)
        {
            Tag tag_1;
            tag_1.tag_name    = tag_db_oc[j]["tag_name"].asString();
            tag_1.description = tag_db_oc[j]["description"].asString();
            tag_1.io_type     = tag_db_oc[j]["io_type"].asString();
            tag_1.data_type   = tag_db_oc[j]["data_type"].asString();
            tag_1.modbus_address = tag_db_oc[j]["modbus_address"].asInt();
            tag_1.alarm_event_enable = tag_db_oc[j]["alarm_event_enable"].asBool();
            tag_1.alarm_hihi = tag_db_oc[j]["alarm_hihi"].asFloat();
            tag_1.alarm_hi = tag_db_oc[j]["alarm_hi"].asFloat();
            tag_1.alarm_lo = tag_db_oc[j]["alarm_lo"].asFloat();
            tag_1.alarm_lolo = tag_db_oc[j]["alarm_lolo"].asFloat();
            tag_1.event_msg_true = tag_db_oc[j]["event_msg_true"].asString();
            tag_1.event_msg_false = tag_db_oc[j]["event_msg_false"].asString();
            tag_1.alarm_priority = tag_db_oc[j]["alarm_priority"].asInt();
            tag_1.log_enable = tag_db_oc[j]["log_enable"].asBool();
             
            tag_1.register_position_index = reg_index_ctr;

            this->tag_database.insert({tag_db_oc[j]["tag_name"].asString(),tag_1}); //add tag to tag_database map structure
            this->tags_to_read.push_back(tag_1);

            coil_io_ctr++;

            reg_index_ctr++;
        }

        //set number of tags count in register
        this->coil_register_count = coil_io_ctr;
        coil_io_ctr = 0;//reset counter


        //analog register tags
        Json::Value tag_db_a;
        tag_db_a = root["plc_tag_db_modbus_map"]["analog_inputs"];

        int analog_input_io_ctr = 0;

        for(Json::Value::ArrayIndex k = 0; k != tag_db_a.size(); k++)
        {
            Tag tag_2;
            tag_2.tag_name    = tag_db_a[k]["tag_name"].asString();
            tag_2.description = tag_db_a[k]["description"].asString();
            tag_2.io_type     = tag_db_a[k]["io_type"].asString();
            tag_2.data_type   = tag_db_a[k]["data_type"].asString();
            tag_2.modbus_address = tag_db_a[k]["modbus_address"].asInt();
            tag_2.alarm_event_enable = tag_db_a[k]["alarm_event_enable"].asBool();
            tag_2.alarm_hihi = tag_db_a[k]["alarm_hihi"].asFloat();
            tag_2.alarm_hi = tag_db_a[k]["alarm_hi"].asFloat();
            tag_2.alarm_lo = tag_db_a[k]["alarm_lo"].asFloat();
            tag_2.alarm_lolo = tag_db_a[k]["alarm_lolo"].asFloat();
            tag_2.event_msg_true = tag_db_a[k]["event_msg_true"].asString();
            tag_2.event_msg_false = tag_db_a[k]["event_msg_false"].asString();
            tag_2.alarm_priority = tag_db_a[k]["alarm_priority"].asInt();
            tag_2.log_enable = tag_db_a[k]["log_enable"].asBool();


            tag_2.register_position_index = reg_index_ctr;

            this->tag_database.insert({tag_db_a[k]["tag_name"].asString(),tag_2}); //add tag to tag_database map structure
            this->tags_to_read.push_back(tag_2);

            if(tag_2.data_type == "UINT")
             analog_input_io_ctr++;
            else if(tag_2.data_type == "REAL")
             analog_input_io_ctr = analog_input_io_ctr + 2;
            else
            {
                //do nothing
            }

            reg_index_ctr++;
        }

        //set number of tags count in register
        this->analog_input_count = analog_input_io_ctr;
        analog_input_io_ctr = 0;//reset counter


        //holding register tags
        Json::Value tag_db_hr;
        tag_db_hr = root["plc_tag_db_modbus_map"]["holding_registers"];

        int holding_register_io_ctr = 0;
        
        for(Json::Value::ArrayIndex m = 0; m != tag_db_hr.size(); m++)
        {
            Tag tag_3;
            tag_3.tag_name    = tag_db_hr[m]["tag_name"].asString();
            tag_3.description = tag_db_hr[m]["description"].asString();
            tag_3.io_type     = tag_db_hr[m]["io_type"].asString();
            tag_3.data_type   = tag_db_hr[m]["data_type"].asString();
            tag_3.modbus_address = tag_db_hr[m]["modbus_address"].asInt();
            tag_3.alarm_event_enable = tag_db_hr[m]["alarm_event_enable"].asBool();
            tag_3.alarm_hihi = tag_db_hr[m]["alarm_hihi"].asFloat();
            tag_3.alarm_hi = tag_db_hr[m]["alarm_hi"].asFloat();
            tag_3.alarm_lo = tag_db_hr[m]["alarm_lo"].asFloat();
            tag_3.alarm_lolo = tag_db_hr[m]["alarm_lolo"].asFloat();
            tag_3.event_msg_true = tag_db_hr[m]["event_msg_true"].asString();
            tag_3.event_msg_false = tag_db_hr[m]["event_msg_false"].asString();
            tag_3.alarm_priority = tag_db_hr[m]["alarm_priority"].asInt();
            tag_3.log_enable = tag_db_hr[m]["log_enable"].asBool();

            tag_3.register_position_index = reg_index_ctr;

            this->tag_database.insert({tag_db_hr[m]["tag_name"].asString(),tag_3}); //add tag to tag_database map structure
            this->tags_to_read.push_back(tag_3);

            if(tag_3.data_type == "UINT")
             holding_register_io_ctr++;
            else if(tag_3.data_type == "REAL")
             holding_register_io_ctr = holding_register_io_ctr + 2;
            else
            {
                //do nothing
            }

            reg_index_ctr++;
        }

        //set number of tags count in register
        this->holding_register_count = holding_register_io_ctr;
        holding_register_io_ctr = 0;//reset counter



        //load Modbus registers start addresses
        Json::Value modbus_register_start_addr;
        modbus_register_start_addr = root["modbus_starting_addresses"];

        for(Json::Value::iterator it_msa = modbus_register_start_addr.begin(); it_msa != modbus_register_start_addr.end();it_msa++)
        {
            Json::Value key_msa = it_msa.key();
            Json::Value value_msa = (*it_msa);

            this->modbus_register_start_addr.insert ({key_msa.asString(),value_msa.asInt()});
        }

    }
    else
    {
      cout<< "Unable to open Tag Database File \n";
      ErrorLog::writeToLog("Unable to open Tag Database File");
    }
}

void IO_Buffer::read_PLC_Data(ModbusDeviceReader* modbus_device_reader)
{
    //read discrete_inputs
     if(!modbus_device_reader->readDiscreteIO("I",this->modbus_register_start_addr["discrete_inputs"],this->discrete_input_count,modbus_device_reader->io_digital_inputs))
     {
         cout<< "Reading of Discrete Inputs on MODBUS Network failed"<<endl;
         ErrorLog::writeToLog("Reading of Discrete Inputs on MODBUS Network failed");
     }

     //read coil_inputs
     if(!modbus_device_reader->readDiscreteIO("C",this->modbus_register_start_addr["output_coils"],this->coil_register_count,modbus_device_reader->io_coil_outputs))
     {
         cout<< "Reading of Coil Outputs on MODBUS Network failed"<<endl;
         ErrorLog::writeToLog("Reading of Coil Outputs on MODBUS Network failed");
     }

     //read analog_inputs
     if(!modbus_device_reader->readAnalogIO("I",this->modbus_register_start_addr["analog_inputs"],this->analog_input_count,modbus_device_reader->io_analog_inputs))
     {
         cout<< "Reading of Analog Inputs on MODBUS Network failed"<<endl;
         ErrorLog::writeToLog("Reading of Analog Inputs on MODBUS Network failed");
     }

     //read holding_register
     if(!modbus_device_reader->readAnalogIO("H",this->modbus_register_start_addr["holding_register"],this->holding_register_count,modbus_device_reader->io_holding_registers))
     {
         cout<< "Reading of Holding Registers on MODBUS Network failed"<<endl;
         ErrorLog::writeToLog("Reading of Holding Registers on MODBUS Network failed");
     }
}

IO_Buffer::io_type_code IO_Buffer::hash_io_type_code(std::string str_io_type)
{
    if(str_io_type == "DISCRETE_INPUT") return DISCRETE_INPUT;
    else if(str_io_type == "COIL_OUTPUT") return COIL_OUTPUT;
    else if(str_io_type == "ANALOG_INPUT") return ANALOG_INPUT;
    else if(str_io_type == "HOLDING_REGISTER") return HOLDING_REGISTER;
    else return UNKNOWN_TYPE;

}

void IO_Buffer::process_PLC_Data_for_HMI(ModbusDeviceReader* modbus_device_reader)
{
    //reset register index positions
    this->coils_register_index_pos = 0;
    this->discrete_input_register_index_pos = 0;
    this->analog_register_index_pos = 0;
    this->holding_register_index_pos = 0;

    //clear data logs vector
    this->logs_to_save.clear();

    std::string current_time = this->getCurrentTime();
    vector<Tag>::iterator itr;

    Json::Value root;
    Json::Value alarm_root;

    //Vector for holding alarm records
    vector<Json::Value> arr_alarm_records;

    for (itr = this->tags_to_read.begin(); itr < this->tags_to_read.end(); itr++)
    {
        Json::Value tag_value_data; //Json Object for holding tag data

        //Json Objects to hold alarm and event data
        Json::Value alarm_record;
        Json::Value event_record;

        //Object of type LogRecord to store tag details to be logged
        LogRecord log_record;
       
        
        //get tag details and retrieve tag values from Modbus registers on board, then format to JSON
        switch(this->hash_io_type_code(itr->io_type))
        {
            case DISCRETE_INPUT:

                   tag_value_data["value"] = std::to_string((int)modbus_device_reader->io_digital_inputs[this->discrete_input_register_index_pos]);
                   tag_value_data["timestamp"] = current_time;
                   cout<<itr->tag_name<<": "<<(int)modbus_device_reader->io_digital_inputs[this->discrete_input_register_index_pos]<<endl;

                   //process alarm status 
                   event_record = this->process_discrete_tag_event_state((int)modbus_device_reader->io_digital_inputs[this->discrete_input_register_index_pos],&(*itr));
                   if(event_record != 0)
                   {
                        //arr_alarm_records.push_back(alarm_record);
                        alarm_root["alarm_and_events"]["events"].append(event_record);
                   }

                    //process logging
                    if(itr->log_enable)
                    {
                        log_record.tag_name = itr->tag_name;
                        log_record.tag_value = std::to_string((int)modbus_device_reader->io_digital_inputs[this->discrete_input_register_index_pos]);
                        log_record.timestamp = current_time;

                        this->logs_to_save.push_back(log_record);
                    }

                   this->discrete_input_register_index_pos++;

            break;

            case COIL_OUTPUT:

                   tag_value_data["value"] = std::to_string((int)modbus_device_reader->io_coil_outputs[this->coils_register_index_pos]);
                   tag_value_data["timestamp"] = current_time;
                   cout<<itr->tag_name<<": "<<(int)modbus_device_reader->io_coil_outputs[this->coils_register_index_pos]<<endl;

                   //process alarm status 
                   event_record = this->process_discrete_tag_event_state((int)modbus_device_reader->io_coil_outputs[this->coils_register_index_pos],&(*itr));
                   if(event_record != 0)
                   {
                        //arr_alarm_records.push_back(alarm_record);
                        alarm_root["alarm_and_events"]["events"].append(event_record);
                   }

                   //process logging
                   if(itr->log_enable)
                   {
                     log_record.tag_name = itr->tag_name;
                     log_record.tag_value = std::to_string((int)modbus_device_reader->io_coil_outputs[this->coils_register_index_pos]);
                     log_record.timestamp = current_time;

                     this->logs_to_save.push_back(log_record);
                   }

                   this->coils_register_index_pos++; 

            break;

            case ANALOG_INPUT:

                   if(itr->data_type == "UINT")
                   {
                       tag_value_data["value"] = std::to_string((int)modbus_device_reader->io_analog_inputs[this->analog_register_index_pos]);
                       tag_value_data["timestamp"] = current_time;
                       cout<<itr->tag_name<<": "<<(int)modbus_device_reader->io_analog_inputs[this->analog_register_index_pos]<<endl;

                       //process alarm status
                       alarm_record = this->process_analog_tag_alarm_state<int>((int)modbus_device_reader->io_analog_inputs[this->analog_register_index_pos],&(*itr));
                       if(alarm_record != 0)
                       {
                           //arr_alarm_records.push_back(alarm_record);
                           alarm_root["alarm_and_events"]["alarms"].append(alarm_record);
                       }

                       //process logging
                       if(itr->log_enable)
                       {
                           log_record.tag_name = itr->tag_name;
                           log_record.tag_value = std::to_string((int)modbus_device_reader->io_analog_inputs[this->analog_register_index_pos]);
                           log_record.timestamp = current_time;

                           this->logs_to_save.push_back(log_record);
                       }

                       this->analog_register_index_pos++;
                   }
                   else if(itr->data_type == "REAL")
                   {
                       tag_value_data["value"] = std::to_string(modbus_device_reader->get_float(modbus_device_reader->io_analog_inputs[this->analog_register_index_pos],modbus_device_reader->io_analog_inputs[this->analog_register_index_pos+1],modbus_device_reader->little_endianness_enable));
                       tag_value_data["timestamp"] = current_time;
                       cout<<itr->tag_name<<": "<<modbus_device_reader->get_float(modbus_device_reader->io_analog_inputs[this->analog_register_index_pos],modbus_device_reader->io_analog_inputs[this->analog_register_index_pos+1],modbus_device_reader->little_endianness_enable)<<endl;
                       
                       //process alarm status
                       alarm_record = this->process_analog_tag_alarm_state<float>(modbus_device_reader->get_float(modbus_device_reader->io_analog_inputs[this->analog_register_index_pos],modbus_device_reader->io_analog_inputs[this->analog_register_index_pos+1],modbus_device_reader->little_endianness_enable),&(*itr));
                       if(alarm_record != 0)
                       {
                           //arr_alarm_records.push_back(alarm_record);
                           alarm_root["alarm_and_events"]["alarms"].append(alarm_record);
                       }

                       //process logging
                       if(itr->log_enable)
                       {
                           log_record.tag_name = itr->tag_name;
                           log_record.tag_value = std::to_string(modbus_device_reader->get_float(modbus_device_reader->io_analog_inputs[this->analog_register_index_pos],modbus_device_reader->io_analog_inputs[this->analog_register_index_pos+1],modbus_device_reader->little_endianness_enable));
                           log_record.timestamp = current_time;

                           this->logs_to_save.push_back(log_record);
                       }

                       this->analog_register_index_pos = this->analog_register_index_pos + 2;
                   }
                   else
                   {
                       //do nothing
                   }

            break;

            case HOLDING_REGISTER:

                   if(itr->data_type == "UINT")
                   {
                       tag_value_data["value"] = std::to_string((int)modbus_device_reader->io_holding_registers[this->holding_register_index_pos]);
                       tag_value_data["timestamp"] = current_time;
                       cout<<itr->tag_name<<": "<<(int)modbus_device_reader->io_holding_registers[this->holding_register_index_pos]<<endl;
                       
                       //process alarm status
                       alarm_record = this->process_analog_tag_alarm_state<int>((int)modbus_device_reader->io_holding_registers[this->holding_register_index_pos],&(*itr));
                       if(alarm_record != 0)
                       {
                           //arr_alarm_records.push_back(alarm_record);
                           alarm_root["alarm_and_events"]["alarms"].append(alarm_record);
                       }

                       //process logging
                       if(itr->log_enable)
                       {
                           log_record.tag_name = itr->tag_name;
                           log_record.tag_value = std::to_string((int)modbus_device_reader->io_holding_registers[this->holding_register_index_pos]);
                           log_record.timestamp = current_time;

                           this->logs_to_save.push_back(log_record);
                       }

                       this->holding_register_index_pos++;
                   }
                   else if(itr->data_type == "REAL")
                   {
                       tag_value_data["value"] = std::to_string(modbus_device_reader->get_float(modbus_device_reader->io_holding_registers[this->holding_register_index_pos],modbus_device_reader->io_holding_registers[this->holding_register_index_pos+1],modbus_device_reader->little_endianness_enable));
                       tag_value_data["timestamp"] = current_time;
                       cout<<itr->tag_name<<": "<<modbus_device_reader->get_float(modbus_device_reader->io_holding_registers[this->holding_register_index_pos],modbus_device_reader->io_holding_registers[this->holding_register_index_pos+1],modbus_device_reader->little_endianness_enable)<<endl;

                       //process alarm status
                       alarm_record = this->process_analog_tag_alarm_state<float>(modbus_device_reader->get_float(modbus_device_reader->io_holding_registers[this->holding_register_index_pos],modbus_device_reader->io_holding_registers[this->holding_register_index_pos+1],modbus_device_reader->little_endianness_enable),&(*itr));
                       if(alarm_record != 0)
                       {
                           //arr_alarm_records.push_back(alarm_record);
                           alarm_root["alarm_and_events"]["alarms"].append(alarm_record);
                       }

                       //process logging
                       if(itr->log_enable)
                       {
                           log_record.tag_name = itr->tag_name;
                           log_record.tag_value = std::to_string(modbus_device_reader->get_float(modbus_device_reader->io_holding_registers[this->holding_register_index_pos],modbus_device_reader->io_holding_registers[this->holding_register_index_pos+1],modbus_device_reader->little_endianness_enable));
                           log_record.timestamp = current_time;

                           this->logs_to_save.push_back(log_record);
                       }

                       this->holding_register_index_pos = this->holding_register_index_pos + 2;
                   }
                   else
                   {
                       //do nothing
                   }

            break;

            default:
                 cout<<"Configuration Error: UNKNOWN IO Type"<<endl;
            break;
        }

        root["TagData"][itr->tag_name] = tag_value_data;//append tag data for a tag to JSON object


    }

    this->tag_values_json = root.toStyledString();

    //build alarms data JSON envelope
    //alarm_root["alarm_and_events"]["alarms"] = arr_alarm_records;
    alarm_root["alarm_and_events"]["timestamp"] = current_time;
    this->alarm_records_json = alarm_root.toStyledString();


}

std::string IO_Buffer::get_tag_db_JSON_Formatted()
{
     //open and read app_settings.json file
    ifstream tag_db_file ("./tag_database.json", ios::binary);

    Json::Value root;
    Json::Reader reader; 

    if (tag_db_file.is_open())
    {
        reader.parse(tag_db_file,root);
        return root.toStyledString();
    }
    else
    {
       cout<< "Unable to open Tag Database File \n";
       return "";
    }
     
}

template <typename T>
Json::Value IO_Buffer::process_analog_tag_alarm_state(T t_value,Tag * tag)
{
    if(tag->alarm_event_enable)
    {
        Json::Value record;

        if(t_value >= tag->alarm_hihi)
        {
            record["tag_name"]          = tag->tag_name;
            record["alarm_description"] = "Hihi Alarm ACTIVE";
            record["value"]             = std::to_string(t_value);
            record["threshold"]         = std::to_string(tag->alarm_hihi);

        }
        else if((t_value >= tag->alarm_hi) && (t_value < tag->alarm_hihi))
        {
            record["tag_name"]          = tag->tag_name;
            record["alarm_description"] = "Hi Alarm ACTIVE";
            record["value"]             = std::to_string(t_value);
            record["threshold"]         = std::to_string(tag->alarm_hi);
        }
        else if((t_value <= tag->alarm_lo) && (t_value > tag->alarm_lolo))
        {
            record["tag_name"]          = tag->tag_name;
            record["alarm_description"] = "Lo Alarm ACTIVE";
            record["value"]             = std::to_string(t_value);
            record["threshold"]         = std::to_string(tag->alarm_lo);
        }
        else if(t_value <= tag->alarm_lolo)
        {
            record["tag_name"]          = tag->tag_name;
            record["alarm_description"] = "Lolo Alarm ACTIVE";
            record["value"]             = std::to_string(t_value);
            record["threshold"]         = std::to_string(tag->alarm_lolo);
        }
        else
        {
            record = 0;
        }

        return record;
    }
    else
     return 0;
}

Json::Value IO_Buffer::process_discrete_tag_event_state(int tag_value,Tag * tag)
{
    if(tag->alarm_event_enable)
    {
        Json::Value record;

        if(tag_value == 1)
        {
            record["tag_name"]          = tag->tag_name;
            record["event_description"] = tag->event_msg_true;
            record["value"]             = std::to_string(tag_value);
            record["threshold"]         = "";
        }
        else if(tag_value == 0)
        {
            record["tag_name"]          = tag->tag_name;
            record["event_description"] = tag->event_msg_false;
            record["value"]             = std::to_string(tag_value);
            record["threshold"]         = "";
        }
        else
        {
            record = 0;
        }

        return record;
    }
    else
        return 0;
}

std::string IO_Buffer::getCurrentTime()
{
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);

    strftime(buf, sizeof(buf),"%Y-%m-%d %X", &tstruct);

    return buf;
}
