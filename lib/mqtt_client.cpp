#include "mqtt_client.h"

using namespace std;

MQTT_Client::MQTT_Client(const char *_id, const char *_topic, const char *_host,int _port,AppSettings *_app_settings,CommandManager *_command_manager) : mosquittopp(_id)
{
    mosqpp::lib_init(); //Initialization for mosquitto library

    //Basic configuration setup
    this->keepalive = 60; 
    this->id = _id;
    this->port =_port;
    this->host = _host;
    this->topic = _topic;
    this->app_settings = _app_settings;
    this->command_manager = _command_manager;
    
    this->isClientConnected = false;

    //set up username and password for connection to broker
    if((this->app_settings->mqtt["mqtt_broker_uid"] != "") && (this->app_settings->mqtt["mqtt_broker_pwd"] != ""))
    {
        username_pw_set(this->app_settings->mqtt["mqtt_broker_uid"].c_str(), this->app_settings->mqtt["mqtt_broker_pwd"].c_str());
    }
    
    //create non blocking connection to broker
    int status = connect_async(host,port,keepalive);

    if(status != MOSQ_ERR_SUCCESS)
     cout << "Not Connected to server at "<< this->host << endl;
    else
     cout << "Connecting to "<< this->host <<".... "<< endl;

    loop_start();//Start thread managing connection / publish / subscribe


}

//destructor
MQTT_Client::~MQTT_Client()
{
   loop_stop();  //kill thread
   mosqpp::lib_cleanup(); //Mosquitto Library cleanup
}

/* void MQTT_Client::set_init_params(AppSettings *_app_settings,CommandManager *_command_manager)
{
    this->app_settings = _app_settings;
    this->command_manager = _command_manager;
} */

void MQTT_Client::on_connect(int rc)
{
    if(rc == 0)
    { 
        //subscribe to device comms channel
        /* char app_str[10]  = "_COMMS";
        char topic_cpy[50]; 

        strcpy(topic_cpy,this->topic); */

        //subscribe(NULL,"IO_SERVER");/*  */
        subscribe(NULL,this->app_settings->mqtt["mqtt_sub_channel"].c_str());
        //const char* tmp = app_str;
        //subscribe(NULL,strcat((char*)this->topic,app_str));
        cout << "Connected to server at "<< this->host <<"\n"<< endl;
        this->isClientConnected = true; 
        OnboardLED::triggerDefaultMode(PWR_LED);
    }
    else
    {
        cout << "Unable to connect server at "<< this->host <<"\n"<< endl; 
        ErrorLog::writeToLog("Unable to connect server at " + (string)this->host);
        this->isClientConnected = false; 
        OnboardLED::triggerHeartbeatMode(PWR_LED);
    }
}

void MQTT_Client::on_disconnect(int rc)
{
    cout << "Disconnected from server at "<< this->host<<"\n"<< endl;
    ErrorLog::writeToLog("Disconnected from server at "+ (string)this->host);
    this->isClientConnected = false; 
    OnboardLED::triggerHeartbeatMode(PWR_LED);
}

void MQTT_Client::on_publish(int mid)
{
    cout << "Message (" << mid <<") was successfully published" << endl;
}

bool MQTT_Client::send_message(const char* _message)
{
    /*  Send message - depending on QoS, mosquitto lib managed re-submission this the thread 
    * NULL : Message Id (int *) this allow to latter get status of each message
    * topic : topic to be used
    * length of the message
    * message
    * qos (0,1,2)
    * retain (boolean) - indicates if message is retained on broker or not
    * Should return MOSQ_ERR_SUCCESS */
    int ret = publish(NULL,this->topic,strlen(_message),_message,0,false);
    return ( ret == MOSQ_ERR_SUCCESS );

}

bool MQTT_Client::send_message_to_any_topic(const char* _message,const char *_topic)
{
    /* Send message - depending on QoS, mosquitto lib managed re-submission this the thread 
    * NULL : Message Id (int *) this allow to latter get status of each message
    * topic : topic to be used
    * length of the message
    * message
    * qos (0,1,2)
    * retain (boolean) - indicates if message is retained on broker or not
    * Should return MOSQ_ERR_SUCCESS */
    int ret = publish(NULL,_topic,strlen(_message),_message,0,false);
    return ( ret == MOSQ_ERR_SUCCESS );

}

void MQTT_Client::on_message(const struct mosquitto_message *message) 
{
    int payload_size = MAX_PAYLOAD + 1;
    char buf[payload_size];

    if(!strcmp(message->topic,this->app_settings->mqtt["mqtt_sub_channel"].c_str()))
    {
        memset(buf, 0, payload_size * sizeof(char));

        //Copy N-1 bytes to ensure always 0 terminated. 
        memcpy(buf, message->payload, MAX_PAYLOAD * sizeof(char));

        //print command received from remote
        cout<<"Command received: "<<endl;
        std::cout << buf << std::endl;

        //JSON Parser objects instantiation
        Json::Value jsonData;
        Json::Reader jsonReader;
        std::string remote_command;

        if (jsonReader.parse(buf, jsonData))
        {
                remote_command = jsonData["remote_command"].asString();

                if(remote_command == "write_to_tag")
                {
                    if(this->command_manager->write_to_tag(jsonData))
                    {
                        //output status here
                    }
                    else
                    {
                        //output status here
                    }
                }
                else if(remote_command == "tag_database")
                {
                    this->command_manager->tag_db_management(jsonData,this);

                    
                }
                else if(remote_command == "system_action")
                {
                    if(this->command_manager->run_system_command(jsonData))
                    {
                        //output status here
                    }
                    else
                    {
                        //output status here
                    }
                }
                else if(remote_command == "application_logs")
                {
                    this->command_manager->program_logs_management(jsonData,this);
                }
                else if(remote_command == "historical_io_data")
                {
                    this->command_manager->historical_io_data_management(jsonData,this);
                }
                else
                {

                }
        }
        else
        {
            cout<<"Command not executed \n";
        }


    }

}

void MQTT_Client::on_subscribe(int mid, int qos_count, const int *granted_qos)
{
        std::cout << "Subscription succeeded." << std::endl;
}













