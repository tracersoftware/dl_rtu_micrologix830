#include <string>
#include <stdlib.h>

#ifndef ONBOARDLED_H
#define ONBOARDLED_H

#define PWR_LED 1
#define ACT_LED 0

class OnboardLED
{
   public:
   
   static void triggerHeartbeatMode(int led_id);
   static void triggerDefaultMode(int led_id);

};

#endif