#include <iostream>
#include <ios>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#ifndef ERRORLOG_H
#define ERRORLOG_H

class ErrorLog
{
   public:

    static std::string getCurrentTime();

    static vector<std::string> logs_buffer;
    static bool write_to_log_enable;

    static void writeToLog(std::string);
    static void writeToDisk();
    static std::string readAll();
    static void clearAll();
};

#endif
