#include "database_manager.h"

using namespace std;

DatabaseManager::DatabaseManager(MYSQL *con)
{
    this->connection = con;
}


void DatabaseManager::connectToDatabase(AppSettings *app_settings)
{
    //inititiate MySQL connection handle
    this->connection = mysql_init(NULL);
    
    //if mysql object initialization fails
    if(this->connection == NULL)
    {
        cout<<"DB Initialization failed \n"<<endl;
        ErrorLog::writeToLog("Database Initialization failed");
        //throw buf;
    }
    else
    {
        cout<<"Db Object initialized"<<"\n";
    }
    
    
    //connect to mysql database, throw exception on failure.
    if(mysql_real_connect(this->connection, app_settings->data_logging["db_host"].c_str(), app_settings->data_logging["db_user"].c_str(), app_settings->data_logging["db_pass"].c_str(), app_settings->data_logging["db_name"].c_str(), 0, NULL, 0) == NULL)
    {
          char buffer[200];
          sprintf(buffer, "Failed to connect to database: Error: %s\n",mysql_error(this->connection));
          cout<<buffer<<endl;
          ErrorLog::writeToLog(buffer);
          //throw buffer;
    }
    else
    {
        cout<<"Db Connection successful"<<"\n";
    }

}

void DatabaseManager::closeDatabaseConnection()
{
    mysql_close(this->connection);
}
