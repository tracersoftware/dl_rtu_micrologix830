#include <stdio.h>
#include <iostream>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <unistd.h>
#include <map>
#include <vector>
#include <bits/stdc++.h>
#include <exception>
#include <pthread.h>
#include <fstream>
#include <mysql.h>
#include "lib/app_settings.cpp"
#include "lib/io_buffer.cpp"
#include "lib/modbus_device_reader.cpp"
#include "lib/tag.cpp"
#include "lib/command_manager.cpp"
#include "lib/mqtt_client.cpp"
#include "lib/data_logger.cpp"
#include "lib/database_manager.cpp"
#include "lib/errorlog.cpp"
#include "lib/onboard_led_control.cpp"

using namespace std;

AppSettings app_settings;
IO_Buffer iobuffer;
modbus_t* context;
MYSQL * con_logger;
DataLogger _data_logger;


ModbusDeviceReader mdr(context,&app_settings);
DatabaseManager db_manager(con_logger);
CommandManager command_mgr(&app_settings,&iobuffer,&mdr,&_data_logger,&db_manager);

void *IO_Logging_Housekeeping(void *void_ptr);
void *ErrorLog_Disk_Writing(void *void_ptr);
//pthread_mutex_t lock;


int main(int argc, char** argv)
{
    int mx=0,my=0,req;
    pthread_t ErrorlogDiskWriter;
    pthread_t IOLogHousekeeper;


    int io_data_logging_interval = 1;

    cout<<"DL RTU Starting"<<endl;
    cout<<"Loading application settings.....";

    //load application settings
    app_settings.load(); 
    cout<<"Loaded"<<endl;

    cout<<"Loading Tag Database.....";
    //load Tag DB
    iobuffer.init_addressing_from_config_file();
    cout<<"Loaded"<<endl;
    
    cout<<"Initializing LED indication.....";

    //set On board LEDs to default state
    OnboardLED::triggerDefaultMode(PWR_LED);
    OnboardLED::triggerDefaultMode(ACT_LED);

    cout<<"Complete"<<endl;

     


    //Setup up Modbus Network
    try
    {
             io_data_logging_interval = stoi(app_settings.data_logging["log_interval"]);//get logging interval

             cout<<"Initializing IO Data Logging database......";
             db_manager.connectToDatabase(&app_settings);
             cout<<"Initializing operation done"<<endl;

             DataLogger data_logger;

             cout<<"Initialized MQTT......";

             MQTT_Client mqtt_c(app_settings.mqtt["mqtt_client_id"].c_str(),app_settings.mqtt["mqtt_pub_channel"].c_str(),app_settings.mqtt["mqtt_broker_address"].c_str(),atoi(app_settings.mqtt["mqtt_broker_port"].c_str()),&app_settings,&command_mgr);
            
             cout<<"Initializing operation done"<<endl;


            //create threads to handle background jobs
            cout<<"Creating IO Logs Housekeeping thread.......";
            if(pthread_create(&IOLogHousekeeper, NULL, IO_Logging_Housekeeping, &mx))
            {
                ErrorLog::writeToLog("Error creating IO Log Housekeeping thread");
                cout<<"failed!"<<endl;
            }
            else
            {
                 cout<<"Successful!"<<endl;
            }

            cout<<"Creating Error Log Diskwriter thread.......";
            if(pthread_create(&ErrorlogDiskWriter, NULL, ErrorLog_Disk_Writing, &my))
            {
                ErrorLog::writeToLog("Error creating IO Log Housekeeping thread");
                cout<<"failed!"<<endl;
            }
            else
            {
                cout<<"Successful"<<endl;
            }



             std::string io_data_json;
             std::string alarms_json;

             mdr.createDevice();
             mdr.initDevice();

             if(!(mdr.connect()))
             {
                cout<<"Modbus Network Setup failed \n";
                ErrorLog::writeToLog("Modbus Network Setup failed");
             }
             else
             {
                cout<<"Connected to Modbus Network \n";
             }


             int elapsed_seconds_ctr = 0;//elapsed seconds counter
            
             while(true)
             {
                 sleep(10); //sleep thread for 1s every loop cycle

                 iobuffer.read_PLC_Data(&mdr); //read data from PLC using the Modbus protocol
                 iobuffer.process_PLC_Data_for_HMI(&mdr);//update tag values & process alarms

                 io_data_json = iobuffer.tag_values_json;//get json formatted tag data
                 alarms_json = iobuffer.alarm_records_json;//get json formatted alarm data

                 mqtt_c.send_message(io_data_json.c_str());//push tag data to MQTT stream
                 mqtt_c.send_message(alarms_json.c_str());//push alarm data to MQTT stream
                 
                 elapsed_seconds_ctr++;

                 //if number of elapsed seconds is equal to specified logging interval, log tag data, reset elapsed seconds counter to zero
                 if(elapsed_seconds_ctr == io_data_logging_interval)
                 {
                    data_logger.saveData(&db_manager,iobuffer.logs_to_save);
                    elapsed_seconds_ctr = 0;
                 }

                 

             }

             pthread_join(IOLogHousekeeper, NULL);
             pthread_join(ErrorlogDiskWriter, NULL);
             db_manager.closeDatabaseConnection();
             mdr.disconnect();

             //set On board LEDs to default state
            OnboardLED::triggerDefaultMode(PWR_LED);
            OnboardLED::triggerDefaultMode(ACT_LED);


    }
    catch(const char* msg)
    {
        cout<<msg<<"\n";
    }



    return 0;
}

void *IO_Logging_Housekeeping(void *void_ptr)
{
    int data_expiry_limit = stoi(app_settings.data_logging["log_data_storage_duration"]);

    for(;;)
    {       
        _data_logger.performCleanUp(&db_manager,data_expiry_limit);
        sleep(3600);
    }

    pthread_exit(NULL);
}

void *ErrorLog_Disk_Writing(void *void_ptr)
{
    for(;;)
    {
        sleep(3);
        ErrorLog::writeToDisk(); //write error logs to disk file every three seconds
    }

    pthread_exit(NULL);
}







